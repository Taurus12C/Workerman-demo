<?php
define('ROOT_PATH',__DIR__);
ini_set('date.timezone','Asia/Shanghai');
require_once ROOT_PATH.'/worker/handle.php';
require_once ROOT_PATH.'/common/common.php';
require_once 'vendor/autoload.php';
worker\base\Container::getInstance()->bind([
    'Mysql'         => worker\tool\Mysql::class,
    'Config'        => worker\tool\Config::class,
]);