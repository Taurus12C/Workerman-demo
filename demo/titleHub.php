<?php
    $redis = new Redis();
    $redis -> connect('127.0.0.1',6379);
    $redis -> auth('');
    $q1 = [
        'title' => '1+1=?',
        'a' => '5',
        'b' => '4',
        'c' => '2',
        'd' => '3',
        'res' => 'c'
    ];

    $q2 = [
        'title' => '5*9=?',
        'a' => '65',
        'b' => '21',
        'c' => '85',
        'd' => '45',
        'res' => 'd'
    ];
    $redis -> lPush('hub',json_encode($q1));
    $redis -> lPush('hub',json_encode($q2));