<?php
namespace app\controller;

use worker\base\controller\Controller;
use worker\base\Container;
use worker\tool\Config;

class MyWorker extends Controller
{

    private $redis;

    public function __construct()
    {
        parent::__construct();
        $this->worker->uidConnects = array();
        $config = Config::getConfig('redis');
        $this->redis = new \Redis();
        $this->redis -> connect($config['host'],$config['port']);
        $this->redis -> auth($config['pass']);
    }

    public function onConnect($connection)
    {
        $id = rand(1,99999);
        $connection->uid = $id;
        $this->worker->uidConnects[$id] = $connection;
        //查看匹配队列是否有玩家排队
        $count = $this->redis->lLen('player');
        if ($count==0){
            //匹配队列空，加入匹配队列
            $this->redis->lPush('player',$id);
        }else{
            //弹出匹配队列尾部玩家
            $player1 = $this->redis->rPop('player');
            //设置双人对战房间
            $group = [$player1,$id];
            //设置房间id
            $room_id = time();
            //给两位玩家的连接上加上房间id标识
            $this->worker->uidConnects[$id]->roomid = $room_id;
            $this->worker->uidConnects[$player1]->roomid = $room_id;
            //存储房间信息
            $this->redis->set($room_id,json_encode($group));
            $this->redis->set($room_id.':status',0);
            $this->setHub($room_id);
            $data = json_decode($this->redis->rPop($room_id.':hub'),true);
            //发送初始化对局数据
            $this->worker->uidConnects[$player1]->send($this->return_json('0',0,$connection->getRemoteIp()));
            $this->worker->uidConnects[$id]->send($this->return_json('0',0,$this->worker->uidConnects[$player1]->getRemoteIp()));
            $this->worker->uidConnects[$player1]->send($this->return_json('1',$room_id,$data));
            $this->worker->uidConnects[$id]->send($this->return_json('1',$room_id,$data));
            $this->redis->set($room_id.':startTime',time()*1000);
        }

    }

    public function onMessage($connection, $data)
    {
        $data = json_decode($data,true);
        switch ($data['code']){
            case 1:

                break;
            case 2:
                $time = ($data['time'] - $this->redis->get($data['group_id'].':startTime'))/1000;
                $score = (10-$time)*10;
                $enemy_uid = array_values(array_diff(json_decode($this->redis->get($data['group_id']),true),[$connection->uid]))[0];
                $connection->send($this->return_json('2',0,$score));
                $this->worker->uidConnects[$enemy_uid]->send($this->return_json('9',0,$score));
                break;
        }
        $this->redis->incr($data['group_id'].':status');
        $this->set_title($data['group_id']);
    }

    public function onClose($connection)
    {
        $this->redis->lRem('player',$connection->uid,0);
        $room_id = $this->worker->uidConnects[$connection->uid]->roomid;
        $winner = json_decode($this->redis->get($room_id),true);
        if($winner) {
            foreach ($winner as $id) {
                $this->worker->uidConnects[$id]->send($this->return_json('4',0,0));
            }
            $this->redis->EXPIRE($room_id,1);
            $this->redis->EXPIRE($room_id.":startTime",1);
            $this->redis->EXPIRE($room_id.":status",1);
            $this->redis->EXPIRE($room_id.":hub",1);
        }
    }

    public function set_title($roomid){
        $id = json_decode($this->redis->get($roomid),true);
        if ($this->redis->get($roomid.':status')==2){
            if ($this->redis->exists($roomid.':hub')==0){
                foreach ($id as $v){
                    $this->worker->uidConnects[$v]->send($this->return_json('5',0,0));
                }
                $this->redis->EXPIRE($roomid,1);
                $this->redis->EXPIRE($roomid.":startTime",1);
                $this->redis->EXPIRE($roomid.":status",1);
                $this->redis->EXPIRE($roomid.":hub",1);
            }else{
                $data = json_decode($this->redis->rPop($roomid.':hub'),true);
                sleep(1);
                foreach ($id as $v){
                    $this->worker->uidConnects[$v]->send($this->return_json('1',$roomid,$data));
                }
                $this->redis->set($roomid.':startTime',time()*1000);
                $this->redis->set($roomid.':status',0);
            }
        }
    }

    public function setHub($roomid){
        $arr = $this->unique_rand(3941,20648,20);
        $mysql = Container::make('Mysql');
        foreach ($arr as $v){
            $data = $mysql->table('title')->where(['id'=>$v])->find('title,a,b,c,d,res');
            $this->redis->lPush($roomid.':hub',json_encode($data));
        }
        $mysql->close();
    }

    function unique_rand($min, $max, $num) {
        $count = 0;
        $return = array();
        while ($count < $num) {
            $return[] = mt_rand($min, $max);
            $return = array_flip(array_flip($return));
            $count = count($return);
        }
        //打乱数组，重新赋予数组新的下标
        shuffle($return);
        return $return;
    }

    function return_json($code,$room_id,$data){
        $res = [
            'code' => $code,
            'group_id' => $room_id,
            'data' => $data
        ];
        return json_encode($res);
    }
}