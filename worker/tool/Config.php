<?php
namespace worker\tool;

use worker\tool\Even;
use worker\tool\observer\LogHandle;


class Config
{
    private static $config = array();

    public static function getConfig($configName){

        if(array_key_exists($configName,self::$config)){
            return self::$config[$configName];
        }

        if (file_exists(ROOT_PATH.'/config/'.$configName.'.php')){
            return require ROOT_PATH.'/config/'.$configName.'.php';
        }

        self::log('没有设置'.$configName.'配置，或者没有找到'.$configName.'配置文件');
        return false;

    }

    public static function setConfig($key,$config){

        if(is_array($config)){
            self::$config[$key] = $config;
            return true;
        }

        self::log('设置'.$key.'配置失败，请使用数组');
        return false;

    }

    private static function log($data){
        $even = new Even();
        $even->add(new LogHandle());
        $even->notify($data);
    }
}