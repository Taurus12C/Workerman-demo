<?php
namespace worker\tool;

use worker\tool\Config;

class Mysql
{

    private $conn;

    private $table;

    private $sql=[
        'where' => null,
        'order' => null,
        'limit' => null,
    ];

    public function __construct()
    {
        $config = $this->getConfig();
        $dsn = 'mysql:dbname='.$config["dbname"].';host='.$config["host"].';port='.$config["port"];
        try {
            $this->conn = new \PDO($dsn, $config['user'], $config['pass']);
            $this->conn->query('SET NAMES utf8');
        } catch(PDOException $e) {
            die('Could not connect to the database:<br/>' . $e);
        }
    }

    private function getConfig(){
        return Config::getConfig('database');
    }

    public function table($table){
        $this->table = $table;
        return $this;
    }

    public function find($fields='*'){
        $querySql = sprintf("SELECT %s FROM %s", $fields, $this->table);
        if(!empty($this->sql['where'])) {
            $querySql .= ' WHERE ' . $this->sql['where'];
        }
        if(!empty($this->sql['orderBy'])) {
            $querySql .= ' ORDER BY ' . $this->sql['order'];
        }
        if(!empty($this->sql['limit'])) {
            $querySql .= ' LIMIT ' . $this->sql['limit'];
        }
        return $this->query($querySql)->fetch(\PDO::FETCH_ASSOC);
    }

    public function where($where){
        if (!is_array($where)){
            return null;
        }
        $where_sql = [];
        foreach ($where as $k => $v){
            $value = addslashes($v);
            $where_sql[] = "$k='$value'";
        }
        if ($this->sql['where']){
            $this->sql['where'] = ' AND '.implode(' AND ',$where_sql);
        }else{
            $this->sql['where'] = implode(' AND ',$where_sql);
        }
        return $this;
    }

    public function order($order){
        $this->sql['order'] = $order;
        return $this;
    }

    public function limit($limit){
        $this->sql['limit'] = $limit;
        return $this;
    }

    public function insert($data) {
        foreach ($data as $key => &$value) {
            $value = addslashes($value);
        }
        $keys = "`".implode('`,`', array_keys($data))."`";
        $values = "'".implode("','", array_values($data))."'";
        $querySql = sprintf("INSERT INTO %s ( %s ) VALUES ( %s )", $this->table, $keys, $values);
        return $this->query($querySql)->rowCount();
    }

    public function delete() {
        $querySql = sprintf("DELETE FROM %s WHERE ( %s )", $this->table, $this->sql['where']);
        return $this->query($querySql)->rowCount();
    }

    public function update($data) {
        $updateFields = [];
        foreach ($data as $key => $value) {
            $up_value = addslashes($value);
            $updateFields[] = "`$key`='$up_value'";
        }
        $updateFields = implode(',', $updateFields);
        $querySql = sprintf("UPDATE %s SET %s", $this->table, $updateFields);

        if(!empty($this->sql['where'])) {
            $querySql .= ' WHERE ' . $this->sql['where'];
        }

        return $this->query($querySql)->rowCount();
    }

    public function query($sql){
        $this->sql=[
            'where' => null,
            'order' => null,
            'limit' => null,
        ];
        return $this->conn->query($sql);
    }

    public function close() {
        return $this->conn = null;
    }

}
