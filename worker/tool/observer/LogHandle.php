<?php


namespace worker\tool\observer;

use worker\tool\Config;

class LogHandle implements IObserver
{
    public function handle($data)
    {
        // TODO: Implement handle() method.
        $config = Config::getConfig('log');
        $file = fopen($config['path'].'/'.date('Ymd',time()).'log','w+') or die('Unable to open runtime');
        $data = $data.date('Y-m-d H:i:s').PHP_EOL;
        fwrite($file,$data);
        fclose($file);
    }
}