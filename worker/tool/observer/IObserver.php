<?php


namespace worker\tool\observer;


interface IObserver
{
    public function handle($data);
}