<?php


namespace worker\tool\observer;


abstract class EvenAbstract
{
    private $object;

    public function add($object)
    {
        $this->object[] = $object;
    }

    public function notify($data)
    {
        foreach ($this->object as $v){
            $v->handle($data);
        }
    }
}