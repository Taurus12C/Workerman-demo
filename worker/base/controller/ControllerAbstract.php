<?php


namespace worker\base\controller;

use Workerman\Worker;
use worker\tool\Config;

abstract class ControllerAbstract
{
    protected $worker;

    public function __construct()
    {
        $config = Config::getConfig('worker');

        $this->worker = new Worker($config['listen'],$config['ssl']);

        $this->worker->count = $config['count'];

        $this->worker->name = $config['name'];

        $this->worker->transport = $config['transport'];

        $this->worker->reusePort = $config['reusePort'];

        Worker::$stdoutFile = $config['stdoutFile'];

        $this->worker->reloadable = $config['reloadable'];

        Worker::$daemonize = $config['daemonize'];
    }

    public function run()
    {
        $this->worker->onConnect = function ($connection){
            $this->onConnect($connection);
        };

        $this->worker->onMessage = function ($connection, $data){
            $this->onMessage($connection, $data);
        };

        $this->worker->onClose = function ($connection){
            $this->onClose($connection);
        };

        $this->worker->onWorkerStart = function($worker) {
            $this->onWorkerStart($worker);
        };

        $this->worker->onWorkerReload = function ($worker) {
            $this->onWorkerReload($worker);
        };

        $this->worker->onBufferFull = function ($connection) {
            $this->onBufferFull($connection);
        };

        $this->worker->onBufferDrain = function ($connection) {
            $this->onBufferDrain($connection);
        };

        $this->worker->onError = function ($connection, $code, $msg) {
            $this->onError($connection, $code, $msg);
        };

        Worker::runAll();
    }

    abstract public function onConnect($connection);

    abstract public function onMessage($connection, $data);

    abstract public function onClose($connection);

    public function onWorkerStart($worker){}

    public function onWorkerReload($worker){}

    public function onBufferFull($connection){}

    public function onBufferDrain($connection){}

    public function onError($connection, $code, $msg){}

}