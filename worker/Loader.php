<?php
namespace worker;

class Loader
{
    public static function autoload($class){
        require ROOT_PATH.'/'.str_replace('\\','/',$class).'.php';
    }
}