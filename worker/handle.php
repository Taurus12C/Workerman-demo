<?php
use worker\base\Container;
use worker\tool\Even;
use worker\tool\observer\LogHandle;
use worker\tool\Config;

    function db($tablename){
        $db = Container::make('Mysql');
        $db->table($tablename);
        return $db;
    }

    function config($name){
        return Config::getConfig($name);
    }

    function createLog($data){
        $even = new Even();
        $even->add(new LogHandle());
        $even->notify($data);
    }