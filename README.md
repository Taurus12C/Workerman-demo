# 使用方法


### [在线演示地址](http://game.zhxxh.cn) 


<font color=#CC0000 size=1>(如果匹配不到人可以自己打开两个页面)</font>

示例是一个对战答题的简单小游戏，逻辑很简单，前端后端游戏实现的逻辑代码有点乱也没有去整理，实现功能就好(主旨也不是为了展示这个游戏，游戏只是示例>.<)。示例只是用于对本项目使用方法的演示，各位大佬可以根据代码查看项目意图。本人菜鸟一枚，还望多多关照。

下载项目后先配置好redis服务和mysql服务首先运行一下demo里面的`titleHub.php`文件，把题库添加到redis里面(虽然只有两题>.<)。
然后启动Workerman服务(如果觉得题目太少同文件下还有一个`title.json`的mongodb数据文件可以自行导入),目录切换至项目根目录下，执行

`php run.php start`

然后打开demo文件夹下的`index.html`文件，打开两个就能看到对战效果啦(^.^)

### 一、目录结构
~~~
|—— app                         应用程序
|—— common                      公共函数
|—— config                      配置文件
|—— demo                        小示例
|—— runtime                     运行时文件
|—— vendor                      composer依赖
|—— worker                      核心文件
|—— base.php                    自动加载基础文件
|—— run.php                     workerman启动文件
~~~

### 二、使用方法

> #### 1、首先根据需求配置config目录下的配置文件
>
> #### 2、在app/controller目录下创建启动类，继承worker\base\controller\Controller基类
>
> #### 3、在启动类中可以实现workerman所有回调属性，只需方法名和workerman官方回调属性名一致
>
> #### 4、复写workerman方法如下：
>>  public function onConnect($connection){}
>>
>>  public function onMessage($connection, $data){}
>>  
>>  public function onClose($connection){}
>>
>>  public function onWorkerStart($worker){}
>>  
>>  public function onWorkerReload($worker){}
>>
>>  public function onBufferFull($connection){}
>>
>>  public function onBufferDrain($connection){}
>>
>>  public function onError($connection, $code, $msg){}
>
> #### 5、数据库使用容器Container::make('Mysql')获取数据库实例，数据库中实现简单的链式操作
>>
>>  table( $table )       选择要执行的表
>>
>>  where($where)       查询条件$where必须传入数组，如`['id'=>$id]`;
>>
>>  order($order)       排序条件$where为字符串，如'id desc'
>>  
>>  limit($limit)       查询返回结果条数，如10
>>
>>  find()              执行查询语句，返回关联数组
>>
>>  insert($data)       插入方法，$data需传入数组，如`['username'=>$username,'pass'=>$pass]`; 返回影响条数
>>  
>>  delete()            删除方法，返回影响条数
>>
>>  update($data)       更新方法，$data需传入数组，更新语句配合where()方法使用，返回影响条数
>>
>>  query($sql)         执行sql方法，$sql直接传入原生sql语句
>
> #### 6、配置类
>>  通过Config::getConfig($config)获取配置,如Config::getConfig('redis')获取config目录下redis.php配置文件
>
> #### 7、助手函数
>>  db($tablename)          数据库助手函数
>>
>>  config($configname)     获取配置文件
>>
>>  createLog($log)         写入日志

### 三、启动方法
> 只需在run.php中引入base.php然后实例化\app\controller\你的启动类(),然后调用run(),控制台执行php start run.php即可。

## 项目还在开发中未彻底完善，项目相关用法文档也会跟进，如有问题可以联系我
