<?php
    return [

        'count'         => 1,

        'name'          => '',

        'listen'        => 'websocket://0.0.0.0:8282',

        'transport'     => 'tcp',

        'ssl' => array(
//             更多ssl选项请参考手册 http://php.net/manual/zh/context.ssl.php
//            'ssl' => array(
//                // 请使用绝对路径
//                'local_cert'                 => '磁盘地址/*.pem', // 也可以是crt文件
//                'local_pk'                   => '磁盘地址/*.key',
//                'verify_peer'                => false,
//                'allow_self_signed' => true, //如果是自签名证书需要开启此选项
//            )
        ),

        'reusePort'     => true,

        'stdoutFile'    => ROOT_PATH.'/runtime/log/worker.log',

        'reloadable'    => true,

        'daemonize'     => true,
    ];